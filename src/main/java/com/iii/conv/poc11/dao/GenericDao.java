package com.iii.conv.poc11.dao;

import java.util.Collection;

public interface GenericDao<PK, T> {
    T save(T T);

    T get(PK id);

    void delete(PK id);

    Collection<T> getAll();
}
