package com.iii.conv.poc11.dao;

import com.iii.conv.poc11.model.Author;

public interface AuthorDao extends GenericDao<String, Author> {
}
