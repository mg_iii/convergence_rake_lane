package com.iii.conv.poc11.dao.impl;

import com.iii.conv.poc11.dao.AuthorDao;
import com.iii.conv.poc11.model.Author;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

public class AuthorDaoImpl implements AuthorDao {

    private static final String GET_AUTH_SQL =
            "select " +
                    "ha.hub_auth_sqn AUTH_SQN, " +
                    "ha.auth_id AUTH_ID, " +
                    "sa.name_orig NAME_ORIGIN, " +
                    "sa.name_en NAME_EN, " +
                    "sa.country COUNTRY, " +
                    "sa.born BORN," +
                    "sa.died DIED " +
                    "from hub_auth ha, sat_author sa " +
                    "where sa.hub_auth_sqn = ha.hub_auth_sqn " +
                    "and sa.sat_end_date is null " +
                    "and auth_id = :authId";

    private static final String SELECT_AYTH_HUB =
            "select hub_auth_sqn from hub_auth where auth_id = :authId";

    private static final String HUB_AUTH_INSERT =
            "insert into hub_auth(hub_auth_sqn, auth_id, hub_auth_dts, hub_auth_src) values (:nexVal, :auth_id, :sysDate, :sourceName)";

    private static final String AUTH_HUB_SEQ_NEXTVALUE =
            "select seq_hub_auth.nextval from dual";

    private static final String SELECT_FROM_AUTH_SAT =
            "select count(*)" +
            "  from sat_author" +
            "  where hub_auth_sqn = :hubAuthSqn" +
            "  and decode(name_orig, :nameOrig, 1) = 1" +
            "  and decode(name_en, :nameEn, 1) = 1" +
            "  and decode(country, :country, 1) = 1" +
            "  and decode(born, :born, 1) = 1" +
            "  and decode(died, :died, 1) = 1";//                    + "    and sat_end_date is null"

    private static final String UPDATE_AUTH_SAT =
            "update sat_author set sat_end_date = :newDate where hub_auth_sqn = :hubAuthSqn ";

    private static final String INSERT_AUTH_SAT =
            "insert into sat_author(hub_auth_sqn, sat_load_dts, sat_end_date, name_orig, name_en, country, born, died, sat_rec_src) " +
            "    values (:hubAuthSqn, :sysdate, null, :nameOrig, :nameEn, :country, :born, :died, :sourceName)";


    private final NamedParameterJdbcTemplate jdbcTemplate;

    public AuthorDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Author save(Author author) {
        List<Map<String, Object>> hubAuthSqnMap = jdbcTemplate.queryForList(SELECT_AYTH_HUB, Collections.singletonMap("authId", author.getId()));

        String hubAuthSqn = null;
        if (hubAuthSqnMap.isEmpty()) {
            hubAuthSqn = jdbcTemplate.queryForObject(AUTH_HUB_SEQ_NEXTVALUE, Collections.emptyMap(), String.class);

            Map<String, Object> params = new HashMap<>();
            params.put("nexVal", hubAuthSqn);
            params.put("auth_id", author.getId());
            params.put("sysDate", LocalDateTime.now());
            params.put("sourceName", "RMS");

            jdbcTemplate.update(HUB_AUTH_INSERT, params);
        } else {
            hubAuthSqn = String.valueOf(hubAuthSqnMap.get(0).get("HUB_AUTH_SQN"));
        }

        Map<String, Object> params = new HashMap<>();
        params.put("hubAuthSqn", hubAuthSqn);
        params.put("sysdate", LocalDateTime.now());
        params.put("nameOrig", author.getOriginalName());
        params.put("nameEn", author.getEnName());
        params.put("country", author.getCountry());
        params.put("born", author.getBorn());
        params.put("died", author.getDied());
        params.put("sourceName", "RMS");

        Number count = jdbcTemplate.queryForObject(SELECT_FROM_AUTH_SAT, params, Number.class);
        if (count.intValue() > 0) {

            Map<String, Object> p = new HashMap<>();
            p.put("newDate", LocalDateTime.now());
            p.put("hubAuthSqn", hubAuthSqn);

            jdbcTemplate.update(UPDATE_AUTH_SAT, p);
        } else {
            jdbcTemplate.update(INSERT_AUTH_SAT, params);
        }


        return null;
    }

    @Override
    public Author get(String id) {
        List<Author> res = jdbcTemplate.query(GET_AUTH_SQL, Collections.singletonMap("authId", id), AuthorRowMapper.INSTANCE);
        return res.isEmpty() ? null : res.get(0);
    }

    @Override
    public void delete(String id) {
    }

    @Override
    public Collection<Author> getAll() {
        return jdbcTemplate.query(GET_AUTH_SQL, Collections.emptyMap(), AuthorRowMapper.INSTANCE);
    }

    private static final class AuthorRowMapper implements RowMapper<Author> {
        private static final RowMapper<Author> INSTANCE = new AuthorRowMapper();

        @Override
        public Author mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Author(
                    rs.getString("AUTH_ID"),
                    rs.getString("NAME_ORIGIN"),
                    rs.getString("NAME_EN"),
                    rs.getString("COUNTRY"),
                    rs.getDate("BORN").toLocalDate(),
                    rs.getDate("DIED").toLocalDate()
            );
        }
    }
}
