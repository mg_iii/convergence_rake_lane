package com.iii.conv.poc11.dao;

import com.iii.conv.poc11.model.Book;

public interface BookDao extends GenericDao<Long, Book> {
}
