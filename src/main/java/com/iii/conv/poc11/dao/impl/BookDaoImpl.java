package com.iii.conv.poc11.dao.impl;

import com.iii.conv.poc11.dao.BookDao;
import com.iii.conv.poc11.model.Book;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class BookDaoImpl implements BookDao {

    private static final String INSERT_SQL = "INSERT INTO BOOK (title, author) VALUES(:title, :author)";
    private static final String SELECT_SQL = "SELECT id, title, author FROM BOOK WHERE id = :id";
    private static final String SELECT_ALL_SQL = "SELECT id, title, author FROM BOOK";
    private static final String SQL_DELETE = "DELETE FROM BOOK WHERE id = :id";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public BookDaoImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Book save(Book book) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();

        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        namedParameters.addValue("title", book.getTitle());
        namedParameters.addValue("author", book.getAuthor());

        jdbcTemplate.update(INSERT_SQL, namedParameters, generatedKeyHolder);

        book.setId(generatedKeyHolder.getKey().longValue());
        return book;
    }

    @Override
    public Book get(Long id) {
        List<Book> res = jdbcTemplate.query(SELECT_SQL, Collections.singletonMap("id", id), BookRowMapper.INSTANCE);

        return res.isEmpty() ? null : res.get(0);
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update(SQL_DELETE, Collections.singletonMap("id", id));
    }

    @Override
    public Collection<Book> getAll() {
        return jdbcTemplate.query(SELECT_ALL_SQL, Collections.emptyMap(), BookRowMapper.INSTANCE);
    }

    private static final class BookRowMapper implements RowMapper<Book> {
        private static final RowMapper<Book> INSTANCE = new BookRowMapper();

        @Override
        public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Book(
                    rs.getLong("id"),
                    rs.getString("title"),
                    rs.getString("author")
            );
        }
    }
}
