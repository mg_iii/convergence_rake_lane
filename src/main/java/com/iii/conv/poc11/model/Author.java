package com.iii.conv.poc11.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class Author {
    private String id;

    private String originalName;
    private String enName;
    private String country;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate born;

    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate died;

    public Author() {
    }

    public Author(String id, String originalName, String enName, String country, LocalDate born, LocalDate died) {
        this.id = id;
        this.originalName = originalName;
        this.enName = enName;
        this.country = country;
        this.born = born;
        this.died = died;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDate getBorn() {
        return born;
    }

    public void setBorn(LocalDate born) {
        this.born = born;
    }

    public LocalDate getDied() {
        return died;
    }

    public void setDied(LocalDate died) {
        this.died = died;
    }
}
