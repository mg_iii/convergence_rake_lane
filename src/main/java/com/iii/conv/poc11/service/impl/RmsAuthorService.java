package com.iii.conv.poc11.service.impl;

import com.iii.conv.poc11.dao.AuthorDao;
import com.iii.conv.poc11.model.Author;
import com.iii.conv.poc11.service.RmsCrudService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Transactional
@RestController
@RequestMapping("/rms/author")
public class RmsAuthorService implements RmsCrudService<Author, String> {

    private AuthorDao authorDao;

    @RequestMapping(method = RequestMethod.POST)
    public Object create(@RequestBody Author author) {
        authorDao.save(author);
        return new ResponseEntity<>(author, HttpStatus.CREATED);
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Author read(@PathVariable String id) {
        return authorDao.get(id);
    }

    @Transactional(readOnly = true)
    @RequestMapping
    public Collection<Author> readAll() {
        return authorDao.getAll();
    }

    public void update(String id, Author newEntity) {
    }

    @Override
    public void delete(String id) {
    }

    public void setAuthorDao(AuthorDao authorDao) {
        this.authorDao = authorDao;
    }
}
