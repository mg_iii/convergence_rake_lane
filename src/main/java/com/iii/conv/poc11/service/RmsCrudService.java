package com.iii.conv.poc11.service;

import java.util.Collection;

public interface RmsCrudService<T, PK> {
    Object create(T entity);

    T read(PK id);

    Collection<T> readAll();

    void update(PK id, T newEntity);

    void delete(PK id);
}
