package com.iii.conv.poc11.service.impl;

import com.iii.conv.poc11.dao.BookDao;
import com.iii.conv.poc11.model.Book;
import com.iii.conv.poc11.service.RmsCrudService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Transactional
@RestController
@RequestMapping("/rms/book")
public class RmsBookService implements RmsCrudService<Book, Long> {

    private BookDao bookDao;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Book book) {
        Book bookWithPk = bookDao.save(book);
        return new ResponseEntity<>(bookWithPk, HttpStatus.CREATED);
    }

    @Transactional(readOnly = true)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Book read(@PathVariable Long id) {
        return bookDao.get(id);
    }

    @Transactional(readOnly = true)
    @RequestMapping
    public Collection<Book> readAll() {
        return bookDao.getAll();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void update(@PathVariable Long id, Book newBook) {
        throw new UnsupportedOperationException("Not yet");
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable Long id) {
        bookDao.delete(id);
    }

    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }
}
