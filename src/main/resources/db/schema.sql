CREATE TABLE BOOK (
  id     NUMBER PRIMARY KEY AUTO_INCREMENT,
  title  VARCHAR,
  author VARCHAR
);

create table hub_title(
  hub_title_sqn number not null,
  title_id varchar2(200 char) not null,
  hub_title_dts date not null,
  hub_title_src varchar2(10) not null,
  constraint pk_hub_title primary key (hub_title_sqn),
  constraint uniq_hub_title_title_id unique (title_id)
);

create sequence seq_hub_title;

create table sat_title(
  hub_title_sqn number not null,
  sat_load_dts date not null,
  sat_end_date date,
  title_orig varchar2(200 char),
  title_en varchar2(200 char),
  title_alter varchar2(200 char),
  sat_rec_src varchar2(10) not null,
  constraint pk_sat_title primary key (hub_title_sqn, sat_load_dts),
  constraint sat_title_fk_hub_title foreign key (hub_title_sqn) references hub_title(hub_title_sqn)
);

create table hub_auth(
  hub_auth_sqn number not null,
  auth_id varchar2(200 char) not null,
  hub_auth_dts date not null,
  hub_auth_src varchar2(10) not null,
  constraint pk_hub_auth primary key (hub_auth_sqn),
  constraint uniq_hub_auth_auth_id unique (auth_id)
);

create sequence seq_hub_auth;

create table sat_author(
  hub_auth_sqn number not null,
  sat_load_dts date not null,
  sat_end_date date,
  name_orig varchar2(200 char),
  name_en varchar2(200 char),
  country varchar2(50 char),
  born date,
  died date,
  sat_rec_src varchar2(10 char) not null,
  constraint pk_sat_author primary key (hub_auth_sqn, sat_load_dts),
  constraint sat_author_fk_hub_auth foreign key (hub_auth_sqn) references hub_auth(hub_auth_sqn)
);

create table lnk_title_author(
  lnk_title_author_sqn number not null,
  hub_title_sqn number not null,
  hub_auth_sqn number not null,
  lnk_title_auth_dts date not null,
  lnk_title_auth_src varchar2(10) not null,
  constraint pk_lnk_title_author primary key (lnk_title_author_sqn),
  constraint lnk_title_author_fk_hub_title foreign key (hub_title_sqn) references hub_title(hub_title_sqn),
  constraint lnk_title_author_fk_hub_auth foreign key (hub_auth_sqn) references hub_auth(hub_auth_sqn)
);

create unique index idx_lnk_title_author_t_a on lnk_title_author(hub_title_sqn, hub_auth_sqn);

create unique index idx_lnk_title_author_a_t on lnk_title_author(hub_auth_sqn, hub_title_sqn);

create sequence seq_lnk_title_author;
