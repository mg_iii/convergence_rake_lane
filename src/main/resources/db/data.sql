insert into HUB_AUTH (hub_auth_sqn, auth_id, hub_auth_dts, hub_auth_src)
values (1001101, 'authId1', to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into HUB_AUTH (hub_auth_sqn, auth_id, hub_auth_dts, hub_auth_src)
values (1001102, 'authId2', to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into HUB_AUTH (hub_auth_sqn, auth_id, hub_auth_dts, hub_auth_src)
values (1001103, 'authId3', to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into HUB_AUTH (hub_auth_sqn, auth_id, hub_auth_dts, hub_auth_src)
values (1001104, 'authId4', to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');

insert into SAT_AUTHOR (hub_auth_sqn, sat_load_dts, sat_end_date, name_orig, name_en, country, born, died, sat_rec_src)
values (1001101, to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), null, 'authId1', 'name1', 'US', to_date('05-11-1900 16:07:21', 'dd-mm-yyyy hh24:mi:ss'), to_date('09-05-1986 07:43:44', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into SAT_AUTHOR (hub_auth_sqn, sat_load_dts, sat_end_date, name_orig, name_en, country, born, died, sat_rec_src)
values (1001102, to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), null, 'authId2', 'name2', 'US', to_date('20-06-1961 05:20:19', 'dd-mm-yyyy hh24:mi:ss'), to_date('12-07-1910 03:33:31', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into SAT_AUTHOR (hub_auth_sqn, sat_load_dts, sat_end_date, name_orig, name_en, country, born, died, sat_rec_src)
values (1001103, to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), null, 'authId3', 'name3', 'RU', to_date('26-01-1955 21:35:47', 'dd-mm-yyyy hh24:mi:ss'), to_date('19-06-1945 01:15:49', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
insert into SAT_AUTHOR (hub_auth_sqn, sat_load_dts, sat_end_date, name_orig, name_en, country, born, died, sat_rec_src)
values (1001104, to_date('24-07-2017 14:49:45', 'dd-mm-yyyy hh24:mi:ss'), null, 'authId4', 'name4', 'RU', to_date('24-02-1905 03:05:31', 'dd-mm-yyyy hh24:mi:ss'), to_date('02-10-1952 18:02:31', 'dd-mm-yyyy hh24:mi:ss'), 'RMS');
